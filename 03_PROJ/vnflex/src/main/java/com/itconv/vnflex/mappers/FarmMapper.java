package com.itconv.vnflex.mappers;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;
import egovframework.rte.psl.dataaccess.util.EgovMap;


@Mapper
public interface FarmMapper{
	/// db 에서 농장 정보 select 한후 list로써 응답 
	public List<EgovMap> selectFarms();

	/// data를 db에 insert (update)한 후 삽입한(업데이트한) 데이터의 id 응답함
	public String insertOrUpdateFarm(Map<String, Object> data);
	
	/// delete farm ('deleted'칼럼 값은 1로 바구고 업데이트한 id 응답함 
	public String deleteFarm (Map<String,Object> data);
}