package com.itconv.vnflex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NongcheongwonWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(NongcheongwonWebApplication.class, args);
	}

}
