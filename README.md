
# VNFLEX project


## folder structure:
	- The Tomcat webserver is located in 03_PROJ\vnflex
	- we need install websquare framework before developing this.
	- the jdk is required for eclipse IDE
## How to deploy?
	- install tomcat webserver
	- from eclipse, right click on project in "project explorer" tab.
	- choose Export - War file - choose location.
	- copy exported war file to tomcat webapps folder, and restart tomcat service.